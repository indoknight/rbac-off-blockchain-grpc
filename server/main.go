package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/wrapperspb"
	//"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"rbac-off-blockchain/server/policyengine"
	pb "rbac-off-blockchain/server/routeguide"
	data "rbac-off-blockchain/server/util"
)

const (
	publicCertFilePath          string = "../../x509/cert.pem"
	privateKeyFilePath          string = "../../x509/private-key.pem" // this is currently unused by the program
	encryptedPrivateKeyFilePath string = "../../x509/private-key-encrypted.pem"
	rsaBlockType                       = "RSA PRIVATE KEY"
)

var (
	tlsEnable         = flag.Bool("tls", true, "Connection uses TLS if true, else plain TCP")
	certFile          = flag.String("cert_file", "", "The TLS cert file")
	keyFile           = flag.String("key_file", "", "The TLS key file")
	port              = flag.Int("port", 9092, "The server port")
	encryptKey []byte = []byte(os.Getenv("CERT_ENC_KEY"))
)

/**
 * This program spins up a TLS enabled gRPC server.
 *
 * encryptPrivateKey is used to first encrypt your private key before starting the program.
 *		E.g. encryptPrivateKey(data.Path("../../x509/<plain-text private key pem file>"))
 * To verify if the encrypted private key, you can decrypt using decryptPrivateKey and
 * compare with original private key.
 */
func main() {
	log.Printf("The env variable value for CERT_ENC_KEY : %v\n", encryptKey)
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	if *tlsEnable {
		if *certFile == "" {
			*certFile = data.Path(publicCertFilePath)
		}
		if *keyFile == "" {
			*keyFile = data.Path(privateKeyFilePath)
		}

		// Load credentials using public certificate and private key files (no support for encrypted private key)
		// credentials, err = credentials.NewServerTLSFromFile(*certFile, *keyFile)

		// Load credentials using public certificate and plaintext or encrypted private key
		credentials, err := loadTLSCredentials()

		if err != nil {
			log.Fatalf("Failed to generate credentials %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(credentials)}
	}
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterRouteGuideServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}

type routeGuideServer struct {
	pb.UnimplementedRouteGuideServer
}

func newServer() *routeGuideServer {
	s := &routeGuideServer{}
	return s
}

// gRPC Implementation
func (s *routeGuideServer) CheckAuthZStatus(ctx context.Context, request *pb.CheckAuthZStatusRequest) (*wrapperspb.BoolValue, error) {
	var allowed = false

	// To print Trace ID is supplied by the client
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		if len(md.Get("X-Org1-Trace-Id")) > 0 {
			traceId := md.Get("X-Org1-Trace-Id")[0]
			log.Printf("The trace Id received : %s\n", traceId)
		} else {
			log.Println("No Trace ID supplied")
		}
	}

	allowed, err := policyengine.CheckAuthZStatus(request.User, request.Action, request.Resource, request.Namespace)
	if err != nil {
		log.Println(err)
		return &wrapperspb.BoolValue{Value: allowed}, nil
	}
	log.Println("Returning AuthZ response")
	return &wrapperspb.BoolValue{Value: allowed}, nil
}

func loadTLSCredentials() (credentials.TransportCredentials, error) {
	// Get public certificate and private key blocks
	cert, _ := tls.X509KeyPair(pem.EncodeToMemory(publicCertBlocks()), privateKeyBlocks())

	return credentials.NewTLS(&tls.Config{Certificates: []tls.Certificate{cert}}), nil
}

func privateKeyBlocks() []byte {
	encKeyFilePath := data.Path(encryptedPrivateKeyFilePath)
	return decryptPrivateKey(encKeyFilePath)
}

func publicCertBlocks() *pem.Block {
	// Cert file
	certFile := data.Path(publicCertFilePath)
	certContent, _ := ioutil.ReadFile(certFile)

	var certBlocks []*pem.Block
	var certBlock *pem.Block

	// Pubic key loading
	for {
		certBlock, certContent = pem.Decode(certContent)
		if certBlock == nil {
			break
		}
		if certBlock.Type != rsaBlockType {
			certBlocks = append(certBlocks, certBlock)
		}
	}
	return certBlocks[0]
}

// Decrypt private key
func decryptPrivateKey(encKeyFilePath string) []byte {
	// Get the encoded private key located at the given file path
	keyContent, _ := ioutil.ReadFile(encKeyFilePath)

	// Decode the encoded private key
	var pkeyDecodedBlock *pem.Block
	var pkeyBytes []byte
	pkeyDecodedBlock, keyContent = pem.Decode([]byte(keyContent))

	if pkeyDecodedBlock.Type == rsaBlockType {

		// Check if the input private key is encrypted
		if x509.IsEncryptedPEMBlock(pkeyDecodedBlock) {
			// Decrypt the encrypted cipher text block
			decryptedBytes, err := x509.DecryptPEMBlock(pkeyDecodedBlock, encryptKey)
			if err != nil {
				log.Fatalf("Error when decrypting the cipher text block : %v", err)
				os.Exit(1)
			}
			log.Println("Successfully decrypted the private key")

			// Write to file to compare with original private key
			//ioutil.WriteFile(data.Path("../../x509/private-key-decrypted.pem"), decryptedBytes, 0400)

			pkeyBytes = decryptedBytes
		} else {
			pkeyBytes = pem.EncodeToMemory(pkeyDecodedBlock)
		}

	} else {
		log.Fatalf("Unsupported block type, make sure the encryption uses block type : %s ", rsaBlockType)
		os.Exit(1)
	}
	return pkeyBytes
}

// Encrypt private key
func encryptPrivateKey(keyFilePath string) {
	// Get the private key located at the given file path
	keyContent, _ := ioutil.ReadFile(keyFilePath)

	// Encrypt the private key
	pkeyEncBlock, err := x509.EncryptPEMBlock(rand.Reader, rsaBlockType, keyContent, encryptKey, x509.PEMCipherAES256)
	if err != nil {
		log.Fatalf("Error when encrypting the private key : %v", err)
	}

	// Verify if the encryption is correct
	verifyEncryption(pkeyEncBlock)

	// Encode the encrypted private key block
	var buff bytes.Buffer
	err = pem.Encode(&buff, pkeyEncBlock)
	if err != nil {
		log.Fatalf("Error when encoding the encrypted private key : %v", err)
	}

	// Write encrypted private key to a file
	ioutil.WriteFile(data.Path(encryptedPrivateKeyFilePath), []byte(buff.String()), 0400)

	log.Println("Successfully encrypted the private key")
}

// check if encryption is successful or not
func verifyEncryption(pkeyEncBlock *pem.Block) {
	if pkeyEncBlock.Type != rsaBlockType {
		log.Fatal("Block type is wrong!")
		os.Exit(1)
	}

	if !x509.IsEncryptedPEMBlock(pkeyEncBlock) {
		log.Fatal("PEM Block is not encrypted!")
		os.Exit(1)
	}
}
