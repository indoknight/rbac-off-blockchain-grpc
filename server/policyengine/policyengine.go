package policyengine

import (
	"bytes"
	"context"
	"fmt"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
	"io/ioutil"
	"log"
	"path/filepath"
	"rbac-off-blockchain/server/util"
)

var partialResultMap = make(map[string]*rego.PartialResult)
var ctx context.Context

// Initialise the Rego policy
func init() {
	initialiseRegoPolicies()
}

// Evaluate the request against the policy. Note that input can be a Struct instance or JSON string
func CheckAuthZStatus(user string, action string, resource string, namespace string) (bool, error) {
	allow := false
	log.Printf("Got this input - user: %s, action: %s, resource: %s and for the namespace: %s\n", user, action, resource, namespace)
	if pr, ok := partialResultMap[namespace]; ok {
		r := pr.Rego(
			rego.Input(map[string]interface{}{
				"user":     user,
				"action":   action,
				"resource": resource,
			}),
		)

		rs, err := r.Eval(ctx)

		if err != nil || len(rs) != 1 || len(rs[0].Expressions) != 1 {
			log.Println(err)
			log.Println("Error when evaluating the request")
		} else {
			outcome := rs[0].Expressions[0].Value
			allow = outcome.(bool)
			log.Println("Evaluated the result to", allow)
		}
		return allow, err
	} else {
		return allow, fmt.Errorf("Invalid namespace provided: %s", namespace)
	}
}

func initialiseRegoPolicies() {
	// read all the namespace policy data files from the policy-data directory
	files, err := ioutil.ReadDir(util.Path("../resources/policy-data"))
	if err != nil {
		log.Fatal(err)
	}
	ctx = context.Background()
	// for each policy data file, create a rego policy and store it in map
	for _, finfo := range files {
		file := finfo.Name()
		dataBytes, err := ioutil.ReadFile(util.Path(filepath.Join("../resources/policy-data", file)))
		if err != nil {
			log.Fatal(err)
		}
		// the namespace is the file name without .json extension
		namespace := file[0 : len(file)-5]
		store := inmem.NewFromReader(bytes.NewBufferString(string(dataBytes)))

		if err != nil {
			log.Fatal(err)
		}
		var policyModel = rego.New(
			rego.Query("data.rbac.policy.allow"),
			rego.Module(namespace, initialPolicy()),
			rego.Store(store),
		)
		pr, err := policyModel.PartialResult(ctx)
		if err != nil {
			log.Fatal(err)
		}
		partialResultMap[namespace] = &pr
		log.Printf("Successfully initialized the policy for the namespace %s", namespace)
	}
}

func initialPolicy() string {
	dataBytes, err := ioutil.ReadFile(util.Path("../resources/policy-definition.rego"))
	if err != nil {
		log.Fatal(err)
	}
	return string(dataBytes)
}
