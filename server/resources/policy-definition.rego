package rbac.policy

import data.user_roles_map
import data.role_permissions_map
import input

default allow = false

# Based on grant and propagate attributes
user_permitted_roles = user_roles_map[input.user]

allow = true {
    # lookup the list of roles for the input user
    userRoles := user_roles_map[input.user]

    # for each user role in that list
    r := userRoles[_]

    # lookup the permissions list for role r
    userPermissions := role_permissions_map[r]

    # for each user permission
    p := userPermissions[_]

    #  check if user request data against the permission
    isIncludedResource(input.resource, p)
    contains(p.operations, input.action)
    not isExcludedResource(input.resource, p)
}

isIncludedResource(iResource, p) {
	startswith(iResource, p.includedObjects)
} else = false

contains(items, element){
  items[_] = element
}

isExcludedResource(iResource, p) {
	p.excludedObjects
    p.excludedObjects != ""
    startswith(iResource, p.excludedObjects[_])
} else = false