module rbac-off-blockchain/server

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/open-policy-agent/opa v0.25.2

	google.golang.org/grpc v1.37.0
    google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
    google.golang.org/protobuf v1.26.0
)
