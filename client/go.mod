module rbac-off-blockchain/client

go 1.15

require (
	google.golang.org/grpc v1.37.1
	google.golang.org/protobuf v1.26.0

	github.com/stretchr/testify v1.5.1
)
