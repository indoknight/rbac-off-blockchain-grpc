package tests

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
	"log"
	"rbac-off-blockchain/client/util"
	"testing"
	"time"

	pb "rbac-off-blockchain/client/routeguide"
	data "rbac-off-blockchain/client/util"
)

var (
	tls                = flag.Bool("tls", true, "Connection uses TLS if true, else plain TCP")
	caFile             = flag.String("ca_file", "", "The file containing the CA root cert file")
	serverAddr         = flag.String("server_addr", "localhost:9092", "The server address in the format of host:port")
	serverHostOverride = flag.String("server_host_override", "", "The server name used to verify the hostname returned by the TLS handshake")
)

type TestData struct {
	Input  pb.CheckAuthZStatusRequest
	Output bool
}

func TestCheckAuthZStatus(t *testing.T) {
	flag.Parse()
	var opts []grpc.DialOption
	if *tls {
		if *caFile == "" {
			*caFile = data.Path("../../x509/cert.pem")
		}
		creds, err := credentials.NewClientTLSFromFile(*caFile, *serverHostOverride)
		if err != nil {
			log.Fatalf("Failed to create TLS credentials %v", err)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(*serverAddr, opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := pb.NewRouteGuideClient(conn)
	log.Printf("Successfully got the client object %v\n\n", client)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// index and value
	log.Println("Input   -----> Response")
	for i, item := range inputData() {
		response, err := client.CheckAuthZStatus(ctx, &item.Input)

		if err != nil {
			log.Fatalf("%v.CheckAuthZStatus(_) = _, %v: ", client, err)
		}
		log.Printf("%v\t\t-----> %v", i, response.GetValue())
		assert.Equal(t, item.Output, response.GetValue(), fmt.Sprintf("%s %d", "The actual output does not match for the test data", i))
	}
}

func inputData() []TestData {
	dataBytes, err := ioutil.ReadFile(util.Path("../resources/test_data.json"))
	if err != nil {
		log.Fatal(err)
	}
	var data []TestData
	err = json.Unmarshal(dataBytes, &data)
	return data
}
