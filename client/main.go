package main

import (
	"context"
	"flag"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	pb "rbac-off-blockchain/client/routeguide"
	data "rbac-off-blockchain/client/util"
)

var (
	tls                = flag.Bool("tls", true, "Connection uses TLS if true, else plain TCP")
	caFile             = flag.String("ca_file", "", "The file containing the CA root cert file")
	serverAddr         = flag.String("server_addr", "localhost:9092", "The server address in the format of host:port")
	serverHostOverride = flag.String("server_host_override", "", "The server name used to verify the hostname returned by the TLS handshake")
)

func main() {
	flag.Parse()
	var opts []grpc.DialOption
	if *tls {
		if *caFile == "" {
			*caFile = data.Path("../../x509/cert.pem")
		}
		creds, err := credentials.NewClientTLSFromFile(*caFile, *serverHostOverride)
		if err != nil {
			log.Fatalf("Failed to create TLS credentials %v", err)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(*serverAddr, opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := pb.NewRouteGuideClient(conn)

	// Check Authz status for the given request
	checkAuthZStatus(client, &pb.CheckAuthZStatusRequest{User: "rosywt", Action: "update", Resource: "/credit/repos", Namespace: "org.europort.corporate"})
}

// Checks AuthZ status for the give input request
func checkAuthZStatus(client pb.RouteGuideClient, request *pb.CheckAuthZStatusRequest) {
	log.Printf("Getting AuthZ status for the request %v", request)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	/*	md, ok := metadata.FromOutgoingContext(ctx)
		if ok{
			md.Set("namespace", "com.org1.projectx")
		}*/

	defer cancel()
	response, err := client.CheckAuthZStatus(ctx, request)
	if err != nil {
		log.Fatalf("%v.GetFeatures(_) = _, %v: ", client, err)
	}
	log.Printf("Received response : %v", response.GetValue())
}
